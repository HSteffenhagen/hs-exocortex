-- almost all of these are there only because of mkYesod
-- this is a pretty annoying thing when setting up Yesod,
-- its generated code needs a lot of language extensions enabled

{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}

import Control.Monad.IO.Class
import Data.Coerce(coerce)
import Data.Maybe(maybe)
import Data.Text(Text)
import Exocortex.Common.Thought
import Yesod
import qualified Database.SQLite.Simple as SQL

data Foundation = Foundation
  { withConnection :: forall a. (SQL.Connection -> IO a) -> IO a
  }

mkYesod "Foundation" [parseRoutes|
  /             HomeR    GET
  /thought/#Int ThoughtR GET
|]

withIndex :: [a] -> [(Int, a)]
withIndex = zip [0..]

getHomeR :: Handler Html
getHomeR = do
  Foundation{..} <- getYesod
  thoughts :: [(Int, Text)] <-  liftIO . withConnection $ \db ->
    SQL.query_ db "SELECT id, title FROM thought ORDER BY title ASC LIMIT 10"
  let thoughtWidget = \ix thoughtTitle -> [whamlet|
    <a href=@{ThoughtR ix}> #{thoughtTitle}
  |]
  defaultLayout [whamlet|
  <h1> List of thoughts
  <ul>
    $forall (thoughtIx, thoughtTitle) <- thoughts
      <li>
        ^{thoughtWidget thoughtIx thoughtTitle}
  |]

getThoughtR :: Int -> Handler Html
getThoughtR ix = do
  Foundation{..} <- getYesod
  thought <- getThoughtOr404 withConnection
  defaultLayout [whamlet|
    <section>
      <header>
        <h1> #{thoughtTitle thought}
      <p> #{thoughtContents thought}
      <footer>
        <a href=@{HomeR}> Back to thought list
  |]
  where
    getThoughtOr404 withConnection = do
      r <- liftIO . withConnection $ \db ->
        SQL.query db "SELECT title, contents FROM thought WHERE thought.id = ?" (SQL.Only ix)
      case r of
        [] -> notFound
        [DbThought thought] -> return thought
        xs -> error "this can't happen because the ID is unique"

instance Yesod Foundation

newtype DbThought = DbThought Thought

instance SQL.FromRow DbThought where
  fromRow = fmap DbThought $ Thought <$> SQL.field <*> SQL.field

instance SQL.ToRow DbThought where
  toRow (DbThought Thought{..}) = SQL.toRow (thoughtTitle, thoughtContents)

-- there are higher level libraries that take care of these things but the
-- queries and relations for this app should be extremely simple, so I'm not
-- sure it's worth dealing with them
-- TODO investigate how much work it'd be to integrate a higher level DB API
migrateDB :: SQL.Connection -> IO ()
migrateDB db = do
  SQL.execute_ db "BEGIN TRANSACTION"
  currentLevel <- getMigrationLevel
  runMigrations currentLevel
  SQL.execute_ db "COMMIT"
  where
    getMigrationLevel = do
      SQL.execute_ db "CREATE TABLE IF NOT EXISTS metavariable (name TEXT PRIMARY KEY, value TEXT)"
      maybeLevel <- SQL.query db "SELECT value FROM metavariable WHERE name = ?" (SQL.Only migrationLevelName)
      case maybeLevel of
        [] -> do
          SQL.execute db "INSERT INTO metavariable (name, value) VALUES (?, ?)" (migrationLevelName, show @Int 0)
          return 0
        [SQL.Only level] -> return $ read level
        xs -> error "This can't happen due to uniqueness constraint on primary key"
    runMigrations currentLevel = mapM_ runMigration . drop currentLevel $ zip [(1::Int) ..] migrations
    runMigration (level, migrate) = do
      migrate
      SQL.executeNamed db "UPDATE metavariable SET value = :value WHERE name = :name"
        [":name" =: migrationLevelName, ":value" =: (level :: Int)]
    migrations = [ addThoughtTable ]
    -- add the thought table and fill it with some dummy values
    addThoughtTable = do
      SQL.execute_ db "CREATE TABLE thought (id INTEGER PRIMARY KEY, title TEXT, contents TEXT)"
      SQL.executeMany db "INSERT INTO thought (title, contents) VALUES (?, ?)" $ coerce @[Thought] @[DbThought]
        [ Thought
          { thoughtTitle = "Hello World"
          , thoughtContents = "this is some content"
          }
        , Thought
          { thoughtTitle = "Some other thought"
          , thoughtContents = "this is just here to check if the list works"
          }
        ]
    migrationLevelName = "migration_level" :: String
    name =: value = name SQL.:= value

main :: IO ()
main = do
  SQL.withConnection "exocortex.db" migrateDB
  warp 3000 Foundation
    -- just create a new connection all the time
    -- TODO find out if this is likely to be a performance problem
    --      and if it is, make sure to pool connections
    { withConnection = SQL.withConnection "exocortex.db"
    }
