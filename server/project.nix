{ mkDerivation
, base
, exocortex-common
, hpack
, sqlite-simple
, text
, yesod
, stdenv
}:
mkDerivation {
  pname = "exocortex-server";
  version = "0.1";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  libraryToolDepends = [ hpack ];
  executableHaskellDepends = [
    base
    exocortex-common
    sqlite-simple
    text
    yesod
  ];
  preConfigure = "hpack";
  homepage = "gitlab.com/HSteffenhagen/hs-exocortex";
  description = "Exocortex HTTP server";
  # BlueOak Model License
  license = "free";
}
