let
  nixpkgs-19-03 = import (builtins.fetchTarball {
    name = "nixpkgs-19.03";
    url = https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz;
    sha256 = "0q2m2qhyga9yq29yz90ywgjbn9hdahs7i8wwlq7b55rdbyiwa5dy";
  });
in
import ./build.nix {
  nixpkgs = nixpkgs-19-03;
}
