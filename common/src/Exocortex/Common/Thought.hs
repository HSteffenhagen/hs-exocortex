module Exocortex.Common.Thought
where

import Data.Text(Text)

data Thought = Thought
  { thoughtTitle :: Text
  , thoughtContents :: Text
  }
  deriving Show
