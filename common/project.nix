{ mkDerivation, base, text, hpack, stdenv }:
mkDerivation {
  pname = "exocortex-common";
  version = "0.1";
  src = ./.;
  libraryHaskellDepends =
    [ base
      text
    ];
  libraryToolDepends = [ hpack ];
  preConfigure = "hpack";
  homepage = "gitlab.com/HSteffenhagen/hs-exocortex";
  description = "Common exocortex library";
  # BlueOak Model License
  license = "free";
}
