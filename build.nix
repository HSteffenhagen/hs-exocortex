{nixpkgs ? import <nixpkgs>}:
let
  config = {
    packageOverrides = pkgs: rec {
      haskellPackages = pkgs.haskellPackages.override {
        overrides = haskellPackagesNew: haskellPackagesOld: rec {
          exocortex-common =
            haskellPackagesNew.callPackage ./common/project.nix {};
          exocortex-server =
            haskellPackagesNew.callPackage ./server/project.nix {};
        };
      };
    };
  };
  pkgs = nixpkgs { inherit config; };
in
{
  exocortex-server = pkgs.haskellPackages.exocortex-server;
}
